import sys
import itertools
import copy


class Graph(object):
    def __init__(self):
        self.vertexes = {}

        # Cache edge pairs when clean
        self._edges = None

    def __deepcopy__(self, memo):
        newone = type(self)()
        newone.__dict__.update(self.__dict__)
        newone.vertexes = copy.deepcopy(self.vertexes, memo)

        return newone

    def __clear__(self):
        """
        :return: None

         Clears cache. Call at every vertex manipulation.

         TODO: Immutable graphs?
        """
        self._edges = None

    @classmethod
    def frompairs(cls, pairs):
        g = cls()

        for v1, v2 in pairs:
            if v1 not in g.vertexes.iterkeys():
                g.vertexes[v1] = set()
            if v2 not in g.vertexes.iterkeys():
                g.vertexes[v2] = set()

            g.vertexes[v1].add(v2)
            g.vertexes[v2].add(v1)

        return g

    @property
    def edges(self):
        if self._edges is not None:
            return self._edges

        done = set()
        edges = set()
        for n,  v in self.vertexes.iteritems():
            edges = edges.union({(n, nei) for nei in v if nei not in done})
            done.add(n)

        self._edges = edges

        return edges

    def copy(self):
        return copy.deepcopy(self)

    def add_vertex(self, name, neighbours):
        """
        :param name: name of the new vertex
        :param neighbours: list of neighbours to the new vertex
        :return: None

        In-place addition to self(V, E).

        It is possible to add multiple vertexes, as the method will add all the
        nonexisting neighbours to the graph.
        """
        self.__clear__()

        neighbours = set(neighbours)
        if name not in self.vertexes:
            self.vertexes[name] = neighbours.copy()
        for n in neighbours:
            if n in self.vertexes:
                self.vertexes[n].add(name)
            else:
                self.vertexes[n] = {name}

    def remove_vertex(self, name):
        """
        :param name: Node to remove
        :return: None

        In-place removal of vertex and all incident edges.
        """
        self.__clear__()

        if name in self.vertexes:
            del self.vertexes[name]

        for n, v in self.vertexes.iteritems():
            if name in v:
                v.remove(name)

    def merge(self, other):
        """
        :param other: Graph B to merge
        :return: None

        In-place addition of B(V', E') to self(V, E).
        """
        self.__clear__()

        for nb, vb in other.vertexes.iteritems():
            self.add_vertex(nb, vb)

    def remove_isolated(self):
        """
        :return: None

        In-place removal of vertexes with rank=0.
        """
        self.__clear__()

        rem = []
        for nb, vb in self.vertexes.iteritems():
            if not vb:
                rem.append(nb)
        for r in rem:
            del self.vertexes[r]

    def subgraph(self, vertexes):
        """
        :param vertexes: vertexes to form subgraph
        :return: New induced subgraph G'(V', E'), where V' is :vertexes: and E' the edges in common from self(V, E).
        """
        Gs = Graph()
        for dn in vertexes:
            Gs.add_vertex(dn, [nn for nn in self.vertexes[dn] if nn in vertexes])

        return Gs

    def complement(self, vertexes):
        """
        :param vertexes: vertexes to form subgraph complement
        :return: New induced subgraph G'(V', E'), where V' is V - :vertexes: from self(V, E).
        """
        diff = set(self.vertexes.keys()).difference(vertexes)
        Gs = Graph()
        for dn in diff:
            Gs.add_vertex(dn, [nn for nn in self.vertexes[dn] if nn in diff])

        return Gs

    def check_vc(self, vc_vertexes):
        for v1, v2 in self.edges:
            if (v1 not in vc_vertexes) and (v2 not in vc_vertexes):
                return False

        return True

    def greedy_vc(self, k):
        a = itertools.permutations([n for n, v in self.vertexes.iteritems()], r=k)
        for x in a:
            if self.check_vc(x):
                return self.subgraph(x)

        raise self.CoverException("[GREEDY] No greedy cover of size %d found" % k)

    def maximal_matching(self):
        """
        :return: Edge pairs of a maximal matching for self(V, E)
        """
        matching = list(self.edges.copy())
        l = 1
        while l < len(matching):
            e1, e2 = matching[l-1]
            delm = []
            for i in range(len(matching) - l):
                m1, m2 = matching[l+i]
                if m1 == e1 or m1 == e2 or m2 == e1 or m2 == e2:
                    delm.append(matching[l+i])
            for d in delm:
                matching.remove(d)
            l += 1

        return matching

    def buss_kernelization(self, k):
        U = {n for n, v in self.vertexes.iteritems() if len(v) > k}
        if len(U) > k:
            raise self.CoverException("[BUSSK] Too many high-rank vertices")

        diff = set(self.vertexes.iterkeys()).difference(U)
        Gs = self.subgraph(diff)
        Gs.remove_isolated()
        VC = self.subgraph(U)
        VC.remove_isolated()

        if len(Gs.edges) > k * (k - len(U)):
            raise self.CoverException("[BUSSK] Too many edges in Gs")

        return Gs, VC

    def __repr__(self):
        return "Graph: %d vertexes" % len(self.vertexes) + "\n" + ", ".join([(str(n)) for n in self.vertexes])

    class CoverException(BaseException):
        pass

###################################################################################################

    def buss_1993(self, k):
        """
        :param k: VC size parameter
        :return: New graph VC, which is the vertex cover of self(V, E)

        Algorithm as per J. F. Buss and J. Goldsmith,
        "Nondeterminism within P*", 1993.
        """
        # Algorithm is mostly reused in others
        Gs, VC = self.buss_kernelization(k)

        VC.merge(Gs.greedy_vc(k - len(VC.vertexes)))
        return VC

    def papadimitriouyannakakis_1996(self, k):
        """
        :param k: VC size parameter
        :return: New graph VC, which is the vertex cover of self(V, E)

        Algorithm as per C. H. Papadimitriou and M. Yannakakis,
        "On limited non-determinism and the complexity of the V-C dimension", 1996.
        """
        Gs, VC = self.buss_kernelization(k)

        matching = Gs.maximal_matching()
        if len(matching) > k:
            raise self.CoverException("[PY1996] Maximal matching too large for k=%d" % k)

        Gu = Graph.frompairs(matching)
        Gm = Gs.complement(Gu.vertexes)
        if 2 * len(matching) <= k:
            return Gu

        # Run through 2^m subsets of matching edges, looking for a VC subgraph match
        candidates = itertools.product(range(2), repeat=len(matching))
        for candidate in candidates:
            vert = {v[s] for v, s in zip(matching, candidate)}
            Gv = Gs.subgraph(vert)
            Gc = Gs.complement(vert)
            try:
                ret = Gc.papadimitriouyannakakis_1996(k-len(matching))
                if ret is not None:
                    # VC of size (k - m) exists
                    cVC = VC.copy()
                    cVC.merge(Gv)

                    # Check leftover vertexes from original complement
                    for ext in Gm.vertexes.iterkeys():
                        for nei in Gs.vertexes[ext]:
                            if nei not in cVC.vertexes.iterkeys():
                                cVC.add_vertex(nei, Gs.vertexes[ext])

                    if len(cVC.vertexes) <= k:
                        VC = cVC
                        break
            except self.CoverException:
                pass

        return VC

    def balasubramanian_1998(self, k, root=None):
        """
        :param k: VC size parameter
        :param root: current root of the search tree
        :return: New graph VC, which is the vertex cover of self(V, E)

        Algorithm as per R. Balasubramanian et al,
        "An improved fixed parameter algorithm for vertex cover", 1998.
        """
        if root is None:
            search, root = self.buss_kernelization(k)
            found = search.balasubramanian_1998(k, root)
            if not self.check_vc(found.vertexes.keys()):
                raise self.CoverException("[BALA1998] No VC found for k=%d" % k)

            return self.subgraph(found.vertexes.keys())

        if len(root.vertexes) > k:
            raise self.CoverException("[BALA1998] No VC found for k=%d" % k)

        if not self.edges:
            return root

        root_core = root.copy()

        # Binary search tree
        ranks = {}
        for vert, neighbours in self.vertexes.iteritems():
            rank = len(neighbours)
            if rank not in ranks:
                ranks[rank] = []
            ranks[rank].append(vert)

        if 1 in ranks:
            for current in ranks[1]:
                try:
                    N = list(self.vertexes[current])[0]
                    return self._b1998_dive(k, root, N)  # N(x)
                except self.CoverException:
                    root = root_core.copy()

            raise self.CoverException("[BALA1998] No path found in 1-chain")

        if 2 in ranks:
            for x in ranks[2]:
                y, z = list(self.vertexes[x])
                if z in self.vertexes[y]:
                    try:
                        return self._b1998_dive(k, root, [y, z])  # {y, z}
                    except self.CoverException:
                        root = root_core.copy()
                else:
                    common = self.vertexes[y].intersection(self.vertexes[z])
                    if len(common) >= 2:
                        try:
                            return self._b1998_dive(k, root, [y, z])  # {y, z}
                        except self.CoverException:
                            root = root_core.copy()
                            try:
                                vertexes = self.vertexes[y].union(self.vertexes[z])
                                return self._b1998_dive(k, root, vertexes)  # N({y, z})
                            except self.CoverException:
                                root = root_core.copy()
                    elif len(common) == 1:
                        a = list(common)[0]
                        try:
                            return self._b1998_dive(k, root, [x, a])  # {x, a}
                        except self.CoverException:
                            root = root_core.copy()

            raise self.CoverException("[BALA1998] No path found in 2-chain")

        if sorted(ranks.keys(), reverse=True)[0] >= 5:
            check = []
            for rank in ranks:
                if rank >= 5:
                    check.append(rank)

            for rank in check:
                for x in ranks[rank]:
                    try:
                        return self._b1998_dive(k, root, x)  # {x}
                    except self.CoverException:
                        root = root_core.copy()
                        try:
                            return self._b1998_dive(k, root, self.vertexes[x])  # N(x)
                        except self.CoverException:
                            root = root_core.copy()

            raise self.CoverException("[BALA1998] No path found in >=5-chain")

        if 3 in ranks:
            for x in ranks[3]:
                Gi = self.subgraph(self.vertexes[x])
                if Gi.edges:
                    try:
                        return self._b1998_dive(k, root, self.vertexes[x])  # {1, 2, 3}
                    except self.CoverException:
                        root = root_core.copy()
                        try:
                            v1, v2 = list(Gi.edges)[0]
                            vertexes = list(self.vertexes[x])
                            vertexes.remove(v1)
                            vertexes.remove(v2)
                            a = vertexes[0]
                            nei = self.vertexes[a]
                            return self._b1998_dive(k, root, nei)  # N(3)
                        except self.CoverException:
                            root = root_core.copy()

                pairs = itertools.combinations(Gi.vertexes.keys(), r=2)
                for n1, n2 in pairs:
                    common = self.vertexes[n1].intersection(self.vertexes[n2])
                    if len(common) == 2:
                        try:
                            return self._b1998_dive(k, root, self.vertexes[x])  # {1, 2, 3}
                        except self.CoverException:
                            root = root_core.copy()
                            common.remove(x)
                            y = list(common)[0]
                            try:
                                return self._b1998_dive(k, root, [x, y])  # {x, y}
                            except self.CoverException:
                                root = root_core.copy()

                for n in Gi.vertexes.keys():
                    nn = self.vertexes[n]
                    if len(nn) > 4:
                        try:
                            return self._b1998_dive(k, root, self.vertexes[x])  # {1, 2, 3}
                        except self.CoverException:
                            root = root_core.copy()
                            try:
                                return self._b1998_dive(k, root, nn)  # N(1)
                            except self.CoverException:
                                root = root_core.copy()
                                try:
                                    vertexes = list(Gi.vertexes.keys())
                                    vertexes.remove(n)
                                    n2, n3 = list(vertexes)
                                    adds = self.vertexes[n2].union(self.vertexes[n3])
                                    adds.add(n)
                                    return self._b1998_dive(k, root, adds)  # {1} U N({2, 3})
                                except self.CoverException:
                                    root = root_core.copy()

                    elif len(nn) == 3:
                        try:
                            return self._b1998_dive(k, root, self.vertexes[x])  # {1, 2, 3}
                        except self.CoverException:
                            root = root_core.copy()
                            vertexes = list(self.vertexes[n])
                            vertexes.remove(x)
                            n4, n5 = list(vertexes)
                            try:
                                return self._b1998_dive(k, root, [x, n4, n5])  # {x, 4, 5}
                            except self.CoverException:
                                root = root_core.copy()
                                try:
                                    vertexes = list(Gi.vertexes.keys())
                                    vertexes.remove(n)
                                    n2, n3 = list(vertexes)
                                    adds = self.vertexes[n2].union(self.vertexes[n3])
                                    adds = adds.union(self.vertexes[n4])
                                    adds = adds.union(self.vertexes[n5])
                                    return self._b1998_dive(k, root, adds)  # N({2, 3, 4, 5})
                                except self.CoverException:
                                    root = root_core.copy()

            raise self.CoverException("[BALA1998] No path found in 3,4-chain")

        if 4 in ranks:
            for x in ranks[4]:
                Gi = self.subgraph(self.vertexes[x])
                if Gi.edges:
                    try:
                        return self._b1998_dive(k, root, self.vertexes[x])  # {1, 2, 3, 4}
                    except self.CoverException:
                        root = root_core.copy()
                        n1, n2 = list(Gi.edges)[0]
                        vertexes = set(self.vertexes[x])
                        vertexes.remove(n1)
                        vertexes.remove(n2)
                        n3, n4 = list(vertexes)
                        try:
                            return self._b1998_dive(k, root, self.vertexes[n3])  # N(3)
                        except self.CoverException:
                            root = root_core.copy()
                            try:
                                cand = self.vertexes[n4]
                                cand.add(n3)
                                return self._b1998_dive(k, root, cand)  # {3} U N(4)
                            except self.CoverException:
                                root = root_core.copy()

                triples = itertools.combinations(Gi.vertexes.keys(), r=3)
                for n1, n2, n3 in triples:
                    common = self.vertexes[n1].intersection(self.vertexes[n2])
                    common = common.intersection(self.vertexes[n3])
                    if len(common) == 2:
                        common.remove(x)
                        y = list(common)[0]
                        try:
                            return self._b1998_dive(k, root, self.vertexes[x])  # {1, 2, 3, 4}
                        except self.CoverException:
                            root = root_core.copy()
                            try:
                                return self._b1998_dive(k, root, [x, y])  # {x, y}
                            except self.CoverException:
                                root = root_core.copy()

                pairs = itertools.combinations(Gi.vertexes.keys(), r=2)
                for n1, n3 in pairs:
                    common = self.vertexes[n1].union(self.vertexes[n3])
                    if len(common) == 6:
                        adj = set(self.vertexes[x])
                        adj.remove(n1)
                        adj.remove(n3)
                        n2, n4 = list(adj)
                        try:
                            return self._b1998_dive(k, root, self.vertexes[x])  # {1, 2, 3, 4}
                        except self.CoverException:
                            root = root_core.copy()
                            try:
                                return self._b1998_dive(k, root, self.vertexes[n2])  # N(2)
                            except self.CoverException:
                                root = root_core.copy()
                                try:
                                    vertexes = set(self.vertexes[n4])
                                    vertexes.add(n2)
                                    return self._b1998_dive(k, root, vertexes)  # {2} U N(4)
                                except self.CoverException:
                                    root = root_core.copy()
                                    try:
                                        adds = self.vertexes[n1].union(self.vertexes[n3])
                                        adds.add(n2)
                                        adds.add(n4)
                                        return self._b1998_dive(k, root, adds)  # {2, 4} U N({1, 3})
                                    except self.CoverException:
                                        root = root_core.copy()

            raise self.CoverException("[BALA1998] No path found in 4-regular-chain")

        raise AssertionError("[BALA1998] No path found")

    def _b1998_dive(self, k, root, nxt):
        """
        Recursion function for balasubramanian_1998
        """
        try:
            for n in nxt:
                root.add_vertex(n, [])
        except TypeError:
            root.add_vertex(nxt, [])
            nxt = [nxt]

        Gs = self.complement(set(nxt))
        Gs.remove_isolated()

        return Gs.balasubramanian_1998(k, root)

###################################################################################################

if __name__ == "__main__":
    from timeit import default_timer as timer

    if len(sys.argv) > 1:
        gf = sys.argv[1]
    else:
        gf = raw_input("Input graph file and press enter: ")
    edges = []
    with open(gf) as gfh:
        for line in gfh:
            edges.append(line.strip().split(" "))

    G = Graph.frompairs(edges)
    print "Loaded Graph from '%s', %d vertexes, %d edges" % (gf, len(G.vertexes), len(G.edges),)

    if len(sys.argv) > 2:
        k = int(sys.argv[2])
    else:
        k = int(raw_input("Input k (cover size) and press enter: "))
    print

    print "Balasubramanian et al(1998):"
    print
    start = timer()
    try:
        res = G.balasubramanian_1998(k)
        print res
    except Graph.CoverException, e:
        print e.message
    print
    print "%.4f s" % (timer() - start)

    print "Papadimitriou-Yannakakis(1996):"
    print
    start = timer()
    try:
        res = G.papadimitriouyannakakis_1996(k)
        print res
    except Graph.CoverException, e:
        print e.message
    print
    print "%.4f s" % (timer() - start)
    print
    print

    print "Buss(1993):"
    print
    start = timer()
    try:
        # Nonviable for any nontrivial graph.
        if k < 8:
            res = G.buss_1993(k)
            print res
        else:
            print "SKIPPED"
    except Graph.CoverException, e:
        print e.message
    print
    print "%.4f s" % (timer() - start)
    print
    print
